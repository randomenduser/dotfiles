stty -ixon # don't stop output with ctrl-s

source $HOME/.shell-helpers/environment-variables

source $HOME/.shell-helpers/aliases
source $HOME/.shell-helpers/prompt-functions
source $HOME/.shell-helpers/colors-and-styles

export PS1='


\[$violet\]$(_hostname_if_remote)\[$resetstyle\] \W\[$cyan\]$(_vc_branch_name_or_blank)\[$orange\]$(_count_jobs_or_blank)\[$resetstyle\] \$ '
