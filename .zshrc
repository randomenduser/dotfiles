stty -ixon # don't stop output with ctrl-s

source $HOME/.shell-helpers/environment-variables

source $HOME/.shell-helpers/aliases
source $HOME/.shell-helpers/prompt-functions
source $HOME/.shell-helpers/colors-and-styles

export HISTFILE="$HOME/.zsh_history"

setopt prompt_subst # allow expansion/substitution in the prompt
setopt interactive_comments # allow comments in interactive mode
setopt menu_complete # select the first match when tab-completing
unsetopt no_match # don't report pattern-matching errors (allows for things like '^')
bindkey -e # use emacs/readline-like keybindings

# enable tab completion
autoload -U compinit && compinit

# allow selecting completions with the arrow keys
zstyle ':completion:*' menu select
# case-insensitive completion
zstyle ':completion:*:complete:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
# use approximate matches when completing (e.g. completes `vim ~/.zsj<tab>` to `vim ~/.zshrc`)
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
# complete using the default colors
zstyle ':completion:*:default' list-colors ''
# don't complete backup files
zstyle ':completion:*' ignored-patterns '*~' '*\\\#*\\\#'

zmodload  zsh/complist
# enter completes the menu item and runs the completed command
bindkey -M menuselect '' accept-line-and-down-history
# shift+tab moves backwards in the menu
bindkey -M menuselect '[Z' reverse-menu-complete

# enable better highlighting if available. The second location is for MacOS with `brew`
if [ -e "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]
then
  source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
elif [ -e "/usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]
then
  source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

export PROMPT='


%{$violet%}$(_hostname_if_remote)%{$resetstyle%} %1~%{$cyan%}$(_vc_branch_name_or_blank)%{$orange%}$(_count_jobs_or_blank)%{$resetstyle%} %# '

# up/down to move through command history (takes into account what was already typed)
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
