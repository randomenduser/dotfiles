-- useful:
--  irc.freenode.net/#xmonad
--  http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Util-EZConfig.html
--    - additionalKeysP key bindings
--  the `xev` command
--    - names which keys are pressed, useful for configuring media keys
--  http://ethanschoonover.com/solarized
--    - colors

import XMonad
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig

myLayout = (smartSpacing 3 $ (smartBorders(Tall 1 (1/20) (5/9)))) |||
           (smartSpacing 3 $ (smartBorders(GridRatio(4/3)))) |||
           (smartSpacing 3 $ (smartBorders(Mirror(Tall 1 (1/20) (7/10)))))


main = xmonad $ ewmh defaultConfig
        { borderWidth        = 2
        , focusedBorderColor = "#d33682" -- 'magenta' from solarized
        , layoutHook         = myLayout
        , modMask            = mod4Mask
        , normalBorderColor  = "#073642" -- 'base02' from solarized
        , terminal           = "urxvt"   -- use rxvt-unicode when launching new terminals
        }
        `additionalKeysP`
        [ 
        -- apps
          ("M-b"                     , spawn "firefox")                                  -- open a web browser
        , ("M-d"                     , spawn "gnome-dictionary --look-up=\"$(xsel)\"")   -- look up a word in the dictionary
        , ("M-f"                     , spawn "thunar")                                   -- open a file browser

        -- music and volume
        , ("M-<Down>"               , spawn "amixer set Master 3%-")
        , ("M-<Left>"               , spawn "cmus-remote --prev")
        , ("M-<Right>"              , spawn "cmus-remote --next")
        , ("M-<Up>"                 , spawn "amixer set Master 3%+")
        , ("<XF86AudioLowerVolume>" , spawn "amixer set Master 3%-")
        , ("<XF86AudioMute>"        , spawn "amixer set Master toggle")
        , ("<XF86AudioNext>"        , spawn "cmus-remote --next")
        , ("<XF86AudioPlay>"        , spawn "cmus-remote --pause")
        , ("<XF86AudioPrev>"        , spawn "cmus-remote --prev")
        , ("<XF86AudioRaiseVolume>" , spawn "amixer set Master 3%+")

        -- os/other
        , ("M-p"                     , spawn ". \"$HOME/.shell-helpers/environment-variables\" && \"$HOME/.bin/dmenu-custom\"")                  -- open websites/terminal apps w/dmenu (https://github.com/losingkeys/dmenu-custom)
        , ("M-S-l"                   , spawn "slock")                        -- lock the screen
        , ("M-S-p"                   , spawn ". \"$HOME/.shell-helpers/environment-variables\" && \"$HOME/.bin/dmenu-custom\" --new-window")     -- open websites/terminal apps in a new window w/dmenu (https://github.com/losingkeys/dmenu-custom)
        , ("M-S-s"                   , spawn "scrot --quality=100")                      -- take a screenshot
        , ("M-S-t"                   , spawn "notify-send \"$(date +'%k:%M %B %e')\"")   -- show the current date and time
        , ("<XF86KbdBrightnessDown>" , spawn "xbacklight -10")
        , ("<XF86KbdBrightnessUp>"   , spawn "xbacklight +10")
        , ("<XF86MonBrightnessDown>" , spawn "xbacklight -10")
        , ("<XF86MonBrightnessUp>"   , spawn "xbacklight +10")
       
        -- workspaces
        , ("M-c"                     , windows copyToAll)                                -- mirror a window to all workspaces
        , ("M-`"                     , toggleWS)                                         -- switch to the most recently used workspace
        ]
