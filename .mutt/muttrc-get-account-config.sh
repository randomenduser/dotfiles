#!/bin/sh

# prompt the user to choose an email account (mutt config for an email
# account) via dmenu

# example account config,
# in the file ~/data/config/mutt-accounts/example:
#
# set from = 'me@example.com'
# set folder = '~/data/mail/example'
# set sendmail = 'sendmail -a example'
# set realname = 'Me'

accountConfFilesDir="$HOME/data/config/mutt-accounts"
if [ -n "$MUTT_ACCOUNTS_DIR" ]; then
  accountConfFilesDir="$MUTT_ACCOUNTS_DIR"
fi

if [ -n "$MUTT_ACCOUNT" ]; then
  accountConfig="$accountConfFilesDir/$MUTT_ACCOUNT"
  if [ ! -r "$accountConfig" ] || [ ! -s "$accountConfig" ] || [ -d "$accountConfig" ]; then
    # TODO: display a warning somehow? (account config missing or empty)
    echo 'exec quit'
    exit 1
  fi

  cat "$accountConfig"
else
  # prompt the user to choose a mailbox if there's more than one. otherwise just load the one
  possibleAccounts="$(find "$accountConfFilesDir" -type f -not -name '#*#' -not -name '*~' -not -name '.*' -exec basename {} \;)"
  accountCount="$(echo "$possibleAccounts" | wc -l)"

  if [ "$accountCount" = "0" ]; then
    # TODO: display a warning somehow? (no account config found)
    echo 'exec quit'
    exit 1
  fi

  accountFilename="$possibleAccounts"
  if [ "$accountCount" != "1" ]; then
    accountFilename="$(echo "$possibleAccounts" | dmenu -i -w "$WINDOWID" -nb teal -nf white -sb orange -sf black)"
  fi

  accountConfig="$accountConfFilesDir/$accountFilename"
  if [ ! -r "$accountConfig" ] || [ ! -s "$accountConfig" ] || [ -d "$accountConfig" ]; then
    # TODO: display a warning somehow? (account config missing or empty (or user quit dmenu))
    echo 'exec quit'
    exit 1
  fi

  cat "$accountConfFilesDir/$accountFilename"
fi
