#!/bin/sh

# add abook contacts to mutt under the group "abook-contacts", which is
# used for contact names and highlighting messages from those contacts

IFS="$(printf '$%b_' '\n')"
for line in $(abook --mutt-query ''); do
  email="$(echo "$line" | cut -f1)"
  name="$(echo "$line" | cut -f2)"

  echo "alias -group abook-contacts '$email' $email ($name)"
done
