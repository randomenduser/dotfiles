#!/bin/sh

# create directories used by mutt

mkdir -p "$HOME/data/cache/mutt/headers"
mkdir -p "$HOME/data/cache/mutt/messages"
mkdir -p "$HOME/data/cache/mutt/tmp"
mkdir -p "$HOME/data/config"
mkdir -p "$HOME/data/mail"
