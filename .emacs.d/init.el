;;; init.el --- Emacs configuration
;;;
;;; Commentary:
;;;   Load ~/.emacs.d/emacs.org (my Emacs config)
;;;
;;; Code:

(require 'package)

(package-initialize)

(setq-default
  package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                      ("melpa" . "https://melpa.org/packages/")
                      ("org" . "https://orgmode.org/elpa/"))
  use-package-always-ensure t)

(setq-default network-security-level 'high)

(unless package-archive-contents (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package)
  (package-install 'diminish))
;;; see https://github.com/jwiegley/use-package#readme for use-package setup info
(eval-when-compile (require 'use-package))
(require 'bind-key)

(use-package org
  :ensure org-plus-contrib
  :diminish footnote-mode
  :bind (("C-c a" . org-agenda)
          ("C-c c" . org-capture)
          ("C-c C-'" . org-edit-special)
          :map org-mode-map
          ;; use SPC/C-SPC to toggle one/all org section(s) in org files
          ;; with a fix so SPC inserts a space in Evil's insert mode
          ("SPC" . (lambda ()
                     (interactive)
                     (if (or (not (boundp 'evil-state)) (eq evil-state 'insert))
                       (insert " ")
                       (org-cycle))))
          ("C-@" . org-shifttab)        ;this is C-SPC
          :map org-src-mode-map
          ("C-c C-'" . org-edit-src-exit))
  :config

  (use-package org-autolist
    :diminish
    :init (add-hook 'org-mode-hook #'org-autolist-mode))

  (defun emacs.org--agenda-and-capture-vars (&rest _)
    "Set org agenda/refile variables based on the most recently accessed open project.
Use a global notes directory if no project is selected. Also
include the PWD for note-taking/research projects."
    (let* ((default-notes-dir "~/notes/")
            (most-recent-project (when (fboundp 'projectile-open-projects) (car (projectile-open-projects))))
            (notes-dir (or most-recent-project default-notes-dir))
            ;; don't allow ~/ to be considered a project directory, use the
            ;; global notes directory instead
            (notes-dir (expand-file-name (if (string= "~/" notes-dir) default-notes-dir notes-dir)))
            (todo-org (concat notes-dir "todo.org"))
            (tasks-dir-org (concat notes-dir "tasks/"))
            (agenda-files (list notes-dir))
            (agenda-files (if (file-exists-p tasks-dir-org)
                            (append agenda-files (list tasks-dir-org))
                            agenda-files)))
      (setq
        org-agenda-files agenda-files
        org-default-notes-file todo-org
        org-refile-targets '((org-agenda-files . (:maxlevel . 2))))))

  ;; project-specific tasks by setting variables based on the most
  ;; recently accessed open project "just in time"
  (advice-add 'org-agenda :before #'emacs.org--agenda-and-capture-vars)
  (advice-add 'org-capture :before #'emacs.org--agenda-and-capture-vars)
  (advice-add 'org-refile :before #'emacs.org--agenda-and-capture-vars)

  (setq-default
    org-confirm-babel-evaluate nil
    org-hide-emphasis-markers t
    org-hide-leading-stars t
    org-log-into-drawer t
    org-todo-keywords '("TODO(t)" "READY(r!)" "BLOCKED(b@/@)" "NEXT(n!)"
                         "|"
                         "DONE(d@/@)" "INVALID(i@/@)"))
  (org-babel-do-load-languages 'org-babel-load-languages'((emacs-lisp . t)
                                                           (restclient . t)
                                                           (rust . t)
                                                           (shell . t)))

  ;; Show a document's outline when opened, instead of just the toplevel
  ;; headings (doesn't show the content)
  (add-hook 'org-mode-hook #'org-content))

(org-babel-load-file (expand-file-name "emacs.org" user-emacs-directory))

(provide 'init)
;;; init.el ends here
