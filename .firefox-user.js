//// ui

// cleanup/increase speed
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.newtabpage.introShown", true);
user_pref("browser.startup.homepage", "about:blank");

// dark theme
user_pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org");


//// security
user_pref("services.sync.engine.passwords", false);

// opt out of shield studies
user_pref("browser.onboarding.shieldstudy.enabled", false);

// don't submit health report data
user_pref("datareporting.healthreport.uploadEnabled", false);


//// other

// disable the builtin pocket addon
user_pref("extensions.pocket.api", "");
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.pocket.oAuthConsumerKey", "");
user_pref("extensions.pocket.site", "");
