module.exports = {
  extends: "airbnb",
  parserOptions: {
    ecmaFeatures: {
      impliedStrict: true,
      jsx: true
    },
    env: {
      es6: true
    }
  }
};
